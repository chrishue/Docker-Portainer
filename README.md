 # Docker-Portainer
  
Docker-Compose stack for Portainer

<!-- TOC -->
- [1. Usage](#1-usage)
  - [1.1. Parameters](#11-parameters)
    - [1.1.1. Ports (Required)](#111-ports-required)
    - [1.1.2. Environments (Optional)](#112-environments-optional)
    - [1.1.3. Traefik Labels - Reverse Proxy](#113-traefik-labels---reverse-proxy)
<!-- /TOC -->

## 1. Usage

### 1.1. Parameters

#### 1.1.1. Ports (Required)

To avoid a already reserved Port issue, add oder append this to `docker-compose.override.yml`.

```yaml
portainer:
  ports:
    # https web admin 
    - 9443:9443
    # tcp tunnel (optional for edge computing)
    - 8000:8000
```
> As Alternative create your own docker-compose prod file and use it like this:
>   `docker-compose -f docker-compose.prod.yml up -d`

#### 1.1.2. Environments (Optional)

If you have to, add this to `docker-compose.override.yml` for the corresponding service as well.

Example:
```yaml
---
version: '3.5'
services:
  portainer:
    environment:
      - TZ=Europe/Zurich
```

#### 1.1.3. Traefik Labels - Reverse Proxy

Minimum adaptations:

Add this to `docker-compose.override.yml`.
* Replace `portainer.yourdomain.com`.
* Replace `yourmiddleware@file`.

```yml
networks:
  traefik-net:
    external: true

services:
  portainer:
    networks:
      - traefik-net
      - stack
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=traefik-net"
      - "traefik.http.routers.portainer.rule=Host(`portainer.yourdomain.com`)"
      - 'traefik.http.routers.portainer.entrypoints=websecure'
      - "traefik.http.routers.portainer.tls=true"
      - "traefik.http.routers.portainer.tls.certresolver=letsencrypt"
      - "traefik.http.routers.portainer.middlewares=yourmiddleware@file"
      - "traefik.http.services.portainer.loadbalancer.server.scheme=https"
      - "traefik.http.services.portainer.loadbalancer.server.port=9443"


